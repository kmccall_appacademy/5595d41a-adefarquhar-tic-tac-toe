class Board
  attr_accessor :grid

  def initialize(grid = Array.new(3) { Array.new(3) })
    @grid = grid
  end

  def place_mark(pos, mark)
    raise "choose another position" unless empty?(pos)
    row, col = pos
    grid[row][col] = mark
  end

  def empty?(pos)
    row, col = pos
    return false unless grid[row][col].nil?
    true
  end

  def winner
    return :X if winning_formartion?(:X)
    return :O if winning_formartion?(:O)

    nil
  end

  def winning_formartion?(mark)
    (0..2).each do |row|
      return true if grid[row].all? { |pos| pos == mark }
    end

    left_diag = [grid[0][0], grid[1][1], grid[2][2]]
    return true if left_diag.all? { |pos| pos == mark }

    right_diag = [grid[0][2], grid[1][1], grid[2][0]]
    return true if right_diag.all? { |pos| pos == mark }

    (0..2).each do |col|
      column = [grid[0][col], grid[1][col], grid[2][col]]
      return true if column.all? { |pos| pos == mark }
    end

    false
  end

  def over?
    return true if winner.is_a? Symbol
    return false if grid.all? { |row| row.all?(&:nil?) }
    return false if grid.any? { |row| row.any?(&:nil?) }

    true
  end
end
