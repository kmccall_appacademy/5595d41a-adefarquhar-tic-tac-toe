class ComputerPlayer
  attr_accessor :mark
  attr_reader :name, :board

  def initialize(name)
    @name = name
    @mark = :O
  end

  def get_move
    return winning_move

  end

  def winning_move
    (0..2).each do |row|
      (0..2).each do |col|
        return [row, pos] if board[row][col].nil?
      end
    end

    left_diag = [board[0][0], board[1][1], board[2][2]]
    return true if left_diag.all? { |pos| pos == mark }

    right_diag = [board[0][2], board[1][1], board[2][0]]
    return true if right_diag.all? { |pos| pos == mark }

    (0..2).each do |col|
      column = [board[0][col], board[1][col], board[2][col]]
      return true if column.all? { |pos| pos == mark }
    end
  end

  def display(board)
    @board = board
  end
end
