class HumanPlayer
  attr_reader :name, :mark

  def initialize(name)
    @name = name
    @mark = :X
  end

  def get_move
    puts "Where do you want to mark (e.g. '0, 1'):"
    move = gets.chomp

    move.delete!("^0-9")
    pos = move.chars.map!(&:to_i)
  end

  def display(board)
    grid = board.grid
    top_row = grid[0].map { |mrk| mrk.nil? ? ' ' : mrk.to_s }
    mid_row = grid[1].map { |mrk| mrk.nil? ? ' ' : mrk.to_s }
    bot_row = grid[2].map { |mrk| mrk.nil? ? ' ' : mrk.to_s }

    puts " #{top_row.join(' | ')}\n#{'-' * 11}"
    puts " #{mid_row.join(' | ')}\n#{'-' * 11}"
    puts " #{bot_row.join(' | ')}"
  end
end
